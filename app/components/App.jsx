import "babel-polyfill";
import Button from './Button'
import RecordRTC from 'recordrtc';
var record = null

class App extends React.Component {
  state = { type: '' }

  componentDidMount() {
    // this.record()
    this.start()
  }


  start = () => {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(stream => {
        var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
        record = RecordRTC(stream, {
          type: 'audio',
          mimeType: 'audio/wav',
          recorderType: StereoAudioRecorder,
          numberOfAudioChannels: 1,
          desiredSampRate: 16 * 1000 
        });
        record.startRecording();
      })
      .catch(err => console.log('Uh oh... unable to get stream...', err));
  }

  stop2 = () => {
    record.stopRecording((audioURL) => {
      var recordedBlob = record.getBlob();
      document.getElementById('audio1').src = URL.createObjectURL(recordedBlob)
      console.log(recordedBlob)
    });
  }

  stop = () => {
    record.stopRecording((audioURL) => {
      var recordedBlob = record.getBlob();
      document.getElementById('audio1').src = URL.createObjectURL(recordedBlob)

      var fileReader = new FileReader();
      fileReader.onload = () => {
        var arrayBuffer = fileReader.result;
        var buff = new Int16Array(fileReader.result)

        var left = [], right = [], i = 0;

        while (i < buff.length) {
          left.push(buff[i]);
          right.push(buff[i + 1]);
          i += 2
        }
        this.test(left, right)
      }
      fileReader.readAsArrayBuffer(recordedBlob);

      console.log(recordedBlob)
    });
  }

  test = (left, right) => {
    const mp3encoder = new lamejs.Mp3Encoder(2, 44100, 128);
    var mp3Data = [];

    console.log(left, right)

    const sampleBlockSize = 1152; //can be anything but make it a multiple of 576 to make encoders life easier

    for (var i = 0; i < left.length; i += sampleBlockSize) {
      const leftChunk = left.splice(i, i + sampleBlockSize);
      const rightChunk = right.splice(i, i + sampleBlockSize);
      var mp3buf = mp3encoder.encodeBuffer(leftChunk, rightChunk);
      if (mp3buf.length > 0) {
        mp3Data.push(mp3buf);
      }
    }

    var mp3buf = mp3encoder.flush();   //finish writing mp3

    if (mp3buf.length > 0) {
      mp3Data.push(mp3buf);
    }

    var blob = new Blob(mp3Data, { type: 'audio/mp3' });
    console.log('MP3 URl: ', blob);
    document.getElementById('audio').src = URL.createObjectURL(blob)

  }

  render() {
    console.log('render app')
    return (
      <div id="content">
        A1<audio src="" controls id="audio1"></audio>
        A2 <audio src="" controls id="audio"></audio>
        <button onClick={this.stop2}>Stop</button>
      </div>
    );
  }
}
export default App
