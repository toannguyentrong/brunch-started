import PropTypes from 'prop-types'
import { connect } from 'react-redux';

const BaseCounter = ({ count = 0, onPlusClick, onMinusClick, asynClick }) => (
  <div>
    <h5><a href="https://redux.js.org/">Redux</a> &amp; <a href="https://facebook.github.io/react/">React</a> Counter</h5>
    <p>
      <button onClick={onMinusClick}>-</button>
      {count}
      <button onClick={onPlusClick}>+</button>
      <button onClick={asynClick}>hello</button>
    </p>
  </div>
);

BaseCounter.propTypes = {
  count: PropTypes.number.isRequired,
  onPlusClick: PropTypes.func.isRequired,
  onMinusClick: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return { count: state.score };
};

const mapDispatchToProps = dispatch => {
  return {
    onPlusClick: () => dispatch({ type: 'INCREMENT' }),
    onMinusClick: () => dispatch({ type: 'DECREMENT' }),
    asynClick: () => dispatch({ type: 'ASYNC' })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BaseCounter)
