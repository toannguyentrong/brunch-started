import { reduxForm, Field, Fields, FieldArray } from 'redux-form'
import Counter from './Counter'
import Button from './Button'
import axios from 'axios'

class Form extends React.Component {
  handle = (e) => {
    const forn = new FormData()
    forn.append('file', e.target.files[0])
    console.log(forn)
  }

  onSubmit = (e) => {
    e.preventDefault()
    let formData = new FormData();
    formData.append('name', 'hehe')
    formData.append('profile_pic', this.file.files[0])
    const config = {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
    }

    var request = new XMLHttpRequest();
    request.open("POST", "http://localhost:3000/upload");
    request.send(formData);
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div id="content">
        <form onSubmit={this.onSubmit} ref={e => this.form = e}>
          <div>
            <label>Name</label>
            <Field name="name" component="input" type="text" />
          </div>
          <div>
            <label>Profile Picture</label>
            <input name="profile_pic" type="file" onChange={this.handle} ref={e => this.file = e} />
          </div>
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'fileupload',
  onSubmit: e => console.log(e)
})(Form)
