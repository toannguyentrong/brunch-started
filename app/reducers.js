import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

const score = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state
  }
}

const fetchLink = () => setTimeout(() => {
  console.log('done')
  return Promise.resolve('helo')
}, 1000)

const common = (state = { isLoading: false }, action) => {
  switch (action.type) {
    case 'LOADING': {
      return { isLoading: action.isLoading }
    }
    case 'ASYNC': {
      console.log('fetching')
      new Promise((res, rej) => {
        fetchLink()
      })
      .then()
    }
    default:
      return state
  }
}

export default combineReducers({
  score,
  common,
  form: formReducer
})
