const express = require('express')
const app = express()
var cors = require('cors')

var bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors())


app.post('/upload', function (req, res) {
    console.log(req.body)
    res.send('the_template');
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))