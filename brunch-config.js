exports.files = {
  javascripts: {
    joinTo: 'app.js',
    // entrypoints: 'app.js'
  },
  stylesheets: {
    joinTo: 'app.css'
  }
}

exports.modules = {
  autoRequire: {
    'app.js': ['initialize']
  }
}

exports.npm = {
  globals: {
    React: 'react'
  }
}

exports.plugins = {
  babel: {
    presets: ['env', 'react'],
    plugins: ['transform-class-properties']
  },
  sass: {
    mode: 'native',
    sourceMapEmbed: true,
    precision: 8,
    modules: {
      generateScopedName: '[name]__[local]___[hash:base64:5]',
      ignore: ['app/styles/*.scss']
    },
  },

}

exports.hot = true;
